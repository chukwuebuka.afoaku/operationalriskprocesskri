﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.EscalationTopShelf.ExcalationService
{
 public   class ExcalationSchedule
    {
        private readonly IScheduler scheduler;
        private string ReconTimeOfDay = ConfigurationManager.AppSettings.Get("ReconTimeOfDay");
        private string ReconUpdateTimeOfDay = ConfigurationManager.AppSettings.Get("ReconUpdateTimeOfDay");

        public ExcalationSchedule()
        {
            NameValueCollection props = new NameValueCollection
        {
            { "quartz.serializer.type", "binary" },
            { "quartz.scheduler.instanceName", "OperationalRiskException.EscalationTopShelf" },
            { "quartz.jobStore.type", "Quartz.Simpl.RAMJobStore, Quartz" },
            { "quartz.threadPool.threadCount", "3" }
        };
            StdSchedulerFactory factory = new StdSchedulerFactory(props);
            scheduler = factory.GetScheduler().ConfigureAwait(false).GetAwaiter().GetResult();
        }
        public void Start()
        {
            scheduler.Start().ConfigureAwait(false).GetAwaiter().GetResult();
            ScheduleJobs();
        }
        public void ScheduleJobs()
        {

            IJobDetail job = JobBuilder.Create<SchedulingImplementation>()
                  .WithIdentity("myJob", "group")
                  .Build();


            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("trigger", "group")
                .WithCronSchedule(ReconTimeOfDay)
                .ForJob("myJob", "group")
                .Build();



            //Update Cron Job
            IJobDetail job1 = JobBuilder.Create<OtherSchedulingImplementation>()
                      .WithIdentity("myJob1", "group1")
                      .Build();
            ITrigger trigger1 = TriggerBuilder.Create()
                .WithIdentity("trigger1", "group1")
                .WithCronSchedule(ReconUpdateTimeOfDay)
                .ForJob("myJob1", "group1")
                .Build();  
            IJobDetail job2 = JobBuilder.Create<SoftWarningSchedulingImplementation>()
                      .WithIdentity("myJob2", "group2")
                      .Build();
            ITrigger trigger2 = TriggerBuilder.Create()
                .WithIdentity("trigger2", "group2")
                .WithCronSchedule(ReconUpdateTimeOfDay)
                .ForJob("myJob2", "group2")
                .Build();

            scheduler.ScheduleJob(job, trigger).ConfigureAwait(false).GetAwaiter().GetResult();
            scheduler.ScheduleJob(job1, trigger1).ConfigureAwait(false).GetAwaiter().GetResult();
            scheduler.ScheduleJob(job2, trigger2).ConfigureAwait(false).GetAwaiter().GetResult();
        }
        public void Stop()
        {
            scheduler.Shutdown().ConfigureAwait(false).GetAwaiter().GetResult();
        }
    }
}

