﻿using Newtonsoft.Json;
using NLog;
using OperationalRiskException.Core.AuthVM;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Services.EntrustService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml.Serialization;

namespace OperationalRiskException.Services.AuthenticationServices
{
    public static class AuthService

    {
        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();
        public static Select2PagedResult GetSelect2PagedResult(string searchTerm, int pageSize, int pageNumber)
        {
            var select2pagedResult = new Select2PagedResult();
            select2pagedResult.Results = GetPagedListOptions(searchTerm, pageSize, pageNumber, out int totalResults);
            select2pagedResult.Total = totalResults;
            return select2pagedResult;
        }

       private static List<Select2OptionModel> GetPagedListOptions(string searchTerm, int pageSize, int pageNumber, out int totalSearchRecords)
        {
            var allSearchedResults = GetAllSearchResults(searchTerm);
            totalSearchRecords = allSearchedResults.Count;
            return allSearchedResults.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
        }
        private static List<Select2OptionModel> GetAllSearchResults(string searchTerm)
        {
            var resultList = new List<Select2OptionModel>();
            if (!string.IsNullOrEmpty(searchTerm))
                resultList = GetSelect2Options().Where(n => n.text.ToLower().Contains(searchTerm.ToLower())).ToList();
            else
                resultList = GetSelect2Options().ToList();
            return resultList;
        }

        private static IQueryable<Select2OptionModel> GetSelect2Options()
        {
            string cacheKey = "Select2Options";
            //check cache
            if (HttpContext.Current.Cache[cacheKey] != null)
            {
                return (IQueryable<Select2OptionModel>)HttpContext.Current.Cache[cacheKey];
            }

            var optionList = new List<Select2OptionModel>();

            HttpClient client = new HttpClient();
            string baseurl = ConfigurationManager.AppSettings["FormsServiceAPIBaseURL"] as string;
            client.BaseAddress = new Uri(baseurl);
            string url = "api/AD/AllUsers";
            var response = client.GetStringAsync(url).Result;
            JavaScriptSerializer js = new JavaScriptSerializer();
            js.MaxJsonLength = Int32.MaxValue;
            //
            var resp = js.Deserialize<List<ADUserVM>>(response);

            

            foreach (var item in resp)
            {
                if (!string.IsNullOrEmpty(item.EmailAddress))
                {
                    optionList.Add(new Select2OptionModel
                    {
                        id = item.EmailAddress,
                        text = item.EmailAddress

                    });
                }

            }

            var result = optionList.AsQueryable();
            //cache results
            HttpContext.Current.Cache[cacheKey] = result;
            return result;
        }


    
        public static  AuthenticationResponce TokenAuthenticateAsync(EntrustRequest _param)
        {
            var ent = new AuthenticationResponce();
            try
            {
                //string Url = ConfigurationManager.AppSettings["Token_ENDPOINT"].ToString();
                //bool Token_Islive = Convert.ToBoolean(ConfigurationManager.AppSettings["Token_Islive"]);
                //string sb = File.ReadAllText(HttpContext.Current.Server.MapPath("~/XML/TokenAuthRequest.xml"));
                //sb = string.Format(sb, _param.response, _param.username);
                //string authentication = "Basic " + Utilities.Utility.Base64Encode(ConfigurationManager.AppSettings["entrustUsername"] + ":" + ConfigurationManager.AppSettings["entrustPss"]);
                //var result = XmlProcessor(Url, sb, Token_Islive, authentication);
                //_loggerInfo.Info("result ::: " + result);
                //using (StringReader stringreader = new StringReader(xmlGetPayload(result)))
                //{
                //    var serializer = new XmlSerializer(typeof(AuthenticationResponce));
                //    var s = (AuthenticationResponce)serializer.Deserialize(stringreader);
                //    return s;
                //}


           
                ServiceClient entrustService;
                BasicHttpBinding binding = new BasicHttpBinding();

                //Specify the address to be used for the client.
                EndpointAddress address =
                   new EndpointAddress("http://10.100.67.247/entrust-svc-v1/Service.svc");
                entrustService = new ServiceClient(binding, address);
                _loggerInfo.Info(" Response::: " + _param.response + " Username::: "+_param.username);
                var checker=entrustService.AuthenticateUser(_param.response, _param.username);
                _loggerInfo.Info( "Token Response::: "+checker);
                if (checker)
                {
                    ent.isSuccessful = "true";
                    ent.response = "Successfully Logged in";

                }
                else {
                    ent.isSuccessful = "false";
                    ent.response = "Failed to Login";
                }
                return ent;
            }
            catch (Exception ex)
            {
                _loggerInfo.Error( " Message:::"+ex.Message + " StackTrace:::" + ex.StackTrace + " InnerException:::" + ex.InnerException, "Error Token");
              
            }
            finally
            {

            }
            return ent;


        }
        private static string xmlGetPayload(string xmlResult)
        {
            string newResult = WebUtility.HtmlDecode(xmlResult);
            _loggerInfo.Info("Response::::" + newResult);
            string retVal = newResult.Substring(newResult.IndexOf("<return>") + 8, newResult.IndexOf("</return>") - (newResult.IndexOf("<return>") + 8));
            return "<?xml version=\"1.0\" encoding=\"utf-8\"?><AuthenticationResponce>" + retVal + "</AuthenticationResponce>";
        }




        public static async Task<bool> ValidateUserOnADAsyncMock(LoginViewModel loginModel)
        {
            await Task.Run(() => { });
            return true;
        }
        public static async Task<bool> ValidateUserOnADAsync(LoginViewModel loginModel)
        {
            bool returnValue = false;
            try
            {
                string urlendpoint = ConfigurationManager.AppSettings["validateuseronAD"];
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(urlendpoint);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = await client.PostAsJsonAsync(urlendpoint, loginModel);
                    returnValue = JsonConvert.DeserializeObject<bool>(response.Content.ReadAsStringAsync().Result);
                }
                loginModel.Password = "";
                //logger.Info("Validate User on AD {0}  and Response {1}", AccountOpening.jsontoString(requestValidation), "" + returnValue);
            }
            catch (Exception ex)
            {
                loginModel.Password = "";
                throw new Exception(ex.Message + "\n" + ex.StackTrace);
                //logger.Info("Validate User on AD {0} ", AccountOpening.jsontoString(requestValidation));
                // logger.Error(exx);
            }
            return returnValue;
        }


        private static string XmlProcessor(string Uri, string xml, bool Token_Islive, string authentication)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Uri);
                
                byte[] bytes;
                _loggerInfo.Info("XML: \n"+xml);
                bytes = System.Text.Encoding.ASCII.GetBytes(xml);
                request.ContentType = "text/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "POST";

                request.UseDefaultCredentials = false;
                request.Proxy = new WebProxy();
                //  request.Headers.Add("Authorization", ConfigurationManager.AppSettings["Token_Authorization"].ToString());
                 //request.Headers.Add("Authorization", authentication);

                if (!Token_Islive)
                {
                    //On Test
                    return ValidResponce();
                }
                else //on Production
                {
                    _loggerInfo.Info("Response API::: Calls requestStream");
                    Stream requestStream = request.GetRequestStream();
                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Close();
                    HttpWebResponse response;
                    response = (HttpWebResponse)request.GetResponse();
                    _loggerInfo.Info("Response API::: "+JsonConvert.SerializeObject(response));
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        Stream responseStream = response.GetResponseStream();
                        var esb_result = new StreamReader(responseStream).ReadToEnd();
                        // logger.Debug("Entrust Response:: " + esb_result);
                        _loggerInfo.Info("esb_result:::" +esb_result);
                        return esb_result;
                    }
                }
            }
            catch (Exception ex)
            {
                _loggerInfo.Error("Message :::"+ex.Message + "/n StackTrace:::"+ex.StackTrace+"/n InnerEx"+ex.InnerException
                    , "Error ::: ");
                //logger.Debug("xmlProcessor:: ex: " + ex.Message + " StackTrace" + ex.StackTrace);
                return InValidResponce();

            }
            return null;
        }
        private static string InValidResponce()
        {
            string sb = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                        "   <soapenv:Body>" +
                        "      <NS1:authenticateTokenResponse xmlns:NS1=\"http://ws.waei.uba.com/\">" +
                        "         <return>" +
                        "            <isSuccessful>false</isSuccessful>" +
                        "            <response>Please provide basic authentication</response>" +
                        "         </return>" +
                        "      </NS1:authenticateTokenResponse>" +
                        "   </soapenv:Body>" +
                        "</soapenv:Envelope>";

            return sb;
        }


        private static string ValidResponce()
        {
            string sb = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                        "   <soapenv:Body>" +
                        "      <NS1:authenticateTokenResponse xmlns:NS1=\"http://ws.waei.uba.com/\">" +
                        "         <return>" +
                        "            <isSuccessful>true</isSuccessful>" +
                        "            <response>Please provide basic authentication</response>" +
                        "         </return>" +
                        "      </NS1:authenticateTokenResponse>" +
                        "   </soapenv:Body>" +
                        "</soapenv:Envelope>";

            return sb;
        }

    }
}
