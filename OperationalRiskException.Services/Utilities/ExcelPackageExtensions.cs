﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Services.Utilities
{
	public static class ExcelPackageExtensions
	{
		public static DataTable ToDataTable(this ExcelPackage package)
		{try { 
			ExcelWorksheet workSheet = package.Workbook.Worksheets[0];
			var table = new DataTable();
			foreach (var firstRowCell in workSheet.Cells[1, 1, 1, workSheet.Dimension.End.Column])
			{
				table.Columns.Add(firstRowCell.Text);
			}
			for (var rowNumber = 2; rowNumber <= workSheet.Dimension.End.Row; rowNumber++)
			{
				var row = workSheet.Cells[rowNumber, 1, rowNumber, workSheet.Dimension.End.Column];
				var newRow = table.NewRow();
				foreach (var cell in row)
				{
					newRow[cell.Start.Column - 1] = cell.Text;
				}
				table.Rows.Add(newRow);
			}
			return table;
			}
			catch (Exception ex) {

				throw new Exception("");
			}
		}
	}
}
