﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using OperationalRiskException.Core.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Services.Utilities
{
	public static class ImportExcelToList<TEntity>
	{
		public static List<TEntity> GetAllRecords( Stream FileUpload, string batchNo, TEntity entity) {

			IDictionary<int, string> mapper = new Dictionary<int, string>();

			
			List<TEntity> records = new List<TEntity>();

			ISheet sheet;
			XSSFWorkbook hssfwb = new XSSFWorkbook(FileUpload);
			sheet = hssfwb.GetSheetAt(0);
			IRow headerRow = sheet.GetRow(0);
			int cellCount = headerRow.LastCellNum;
			//getting the header
			for (int j = 0; j < cellCount; j++)
			{
				ICell cell = headerRow.GetCell(j);
				if (cell == null || string.IsNullOrWhiteSpace(cell.ToString())) continue;
				mapper.Add(j, cell.ToString());
				//headers.Add(cell.ToString());
			}
			for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
			{
			
				IRow row = sheet.GetRow(i);
				if (row == null) continue;
				if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
				for (int j = row.FirstCellNum; j < cellCount; j++)
				{
				
					
					if (row.GetCell(j) != null)
					{
						
						foreach (KeyValuePair<int, string> item in mapper)
						{

							var record = entity.GetType().GetProperties().Where(x => x.Name ==item.Value).FirstOrDefault();

							if (record != null) {
								entity.GetType().GetProperties().Where(x => x.Name == item.Value).FirstOrDefault().SetValue(entity, row.GetCell(j).ToString());
							}
							Console.WriteLine("Key: {0}, Value: {1}", item.Key, item.Value);
						}
					
					}
						
					//	sb.Append("<td>" + row.GetCell(j).ToString() + "</td>");
				}
				entity.GetType().GetProperty("BatchNo").SetValue(entity, batchNo);
				//entity.GetType().GetProperty("CreatedDate").SetValue(entity,Helpers.CurrentDateTime);
				records.Add(entity);
				//sb.AppendLine("</tr>");
			}
			return records;
		}
	}
}
