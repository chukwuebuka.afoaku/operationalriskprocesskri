﻿using Newtonsoft.Json;
using NLog;
using OperationalRiskException.Core.AuthVM;
using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Data.GenericRepository.IRepository;
using OperationalRiskException.Data.GenericRepository.Repository;
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Services.Services
{
    public class AssessmentSetupService : IAssessmentSetupService
    {
        private IRepository<AssessmentSetup> _accessmentRepository;
        private IRepository<QuestionCategory> _questionRepository;
        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();
        public AssessmentSetupService()
        {
            _accessmentRepository = new Repository<AssessmentSetup>();
            _questionRepository = new Repository<QuestionCategory>();
        }

        public async Task<bool> CreateAssessment(AssessmentSetupVM model)
        {
            try {

                var checkExisted = _accessmentRepository.GetSingleRecordFilter(x => x.AssessmentName == model.AssessmentName && !x.IsDeleted);
               if (checkExisted!=null)
                {
                    _loggerInfo.Info(JsonConvert.SerializeObject(checkExisted), "Setup Assessment (Create) ::checkExisted");
                    return false;
                }
                    var addEntity = new AssessmentSetup {
                    QuestionCategories = string.Join(",", model.ListOfQuestionCategories),
                AssessmentName =model.AssessmentName,
                    StartDate=model.StartDate,
                    EndDate=model.EndDate,

            };
           _accessmentRepository.Insert(addEntity);
            var dbCheck = await _accessmentRepository.SaveAsync();
            return dbCheck == 1;
            }
            catch (Exception ex) {

                _loggerInfo.Error(ex.ToString(), "Setup Assessment (Create)");
                return false;
            }
        }
        public Select2PagedResult GetAllQuestionCategories(string searchTerm, int pageSize, int pageNumber)
        {
            var select2pagedResult = new Select2PagedResult
            {
                Results = GetPagedListOptions(searchTerm, pageSize, pageNumber, out int totalResults),
                Total = totalResults
            };
            return select2pagedResult;
        }

        private List<Select2OptionModel> GetPagedListOptions(string searchTerm, int pageSize, int pageNumber, out int totalResults)
        {
            var record = _questionRepository.GetAllIncluding(x => !x.IsDeleted, false);
            if (!String.IsNullOrEmpty(searchTerm))
                record = record.Where(x => x.CategoryName.ToLower().Contains(searchTerm.ToLower()));
            totalResults = record.Count();
            record = record.OrderBy(x => x.CategoryName);
            var result = record.Skip(pageNumber-1).Take(pageSize).ToList();
            return result.Select(x => new Select2OptionModel
            {
                id = x.Id.ToString(),
                text = x.CategoryName
            }).ToList();

        }

        public async Task<bool> UpdateAssessment(AssessmentSetupVM assessmentVM)
        {

            try
            {
                var setupEntity = _accessmentRepository.GetAllIncluding(x => !x.IsDeleted && x.Id == assessmentVM.Id, true).FirstOrDefault();
                setupEntity.QuestionCategories = string.Join(",", assessmentVM.ListOfQuestionCategories); 
                setupEntity.StartDate = assessmentVM.StartDate;
                setupEntity.EndDate = assessmentVM.EndDate;
                _accessmentRepository.Update(setupEntity);
                var check = await _accessmentRepository.SaveAsync();
                return check >= 1;
            }
            catch (Exception ex)
            {
                _loggerInfo.Error(ex, "Setup Assessment (Update)");
                return false;
            }
        }


        public async Task<bool> UpdateAddUserToAccessment(AssessmentSetupVM assessmentVM)
        {

            try
            {
                var setupEntity = _accessmentRepository.GetAllIncluding(x => !x.IsDeleted && x.Id == assessmentVM.Id, true).FirstOrDefault();
               
                //setupEntity.Users.Add()
             ///   setupEntity.QuestionCategories = string.Join(",", assessmentVM.ListOfQuestionCategories);
                setupEntity.StartDate = assessmentVM.StartDate;
                setupEntity.EndDate = assessmentVM.EndDate;
                _accessmentRepository.Update(setupEntity);
                var check = await _accessmentRepository.SaveAsync();
                return check == 1;
            }
            catch (Exception ex)
            {
                _loggerInfo.Error(ex, "Setup Assessment (Update)");
                return false;
            }
        }

        public async Task<bool> Delete(AssessmentSetupVM assessmentVM)
        {
            try
            {
                var setupEntity = _accessmentRepository.GetAllIncluding(x => !x.IsDeleted && x.Id == assessmentVM.Id, true).FirstOrDefault();
                _accessmentRepository.Delete(setupEntity);
                var check = await _accessmentRepository.SaveAsync();
                return check == 1;
            }
            catch (Exception ex)
            {
                _loggerInfo.Error(ex, "Setup Assessment (Delete)");
                return false;
            }
        }

        public AssessmentSetupVM GetAssessmentSetup(int Id)
        {
            var setupEntity = _accessmentRepository.GetSingleRecordFilter(x => x.Id == Id);
            var ent = new AssessmentSetupVM {
                ListOfQuestionCategories=setupEntity.QuestionCategories.Split(','),
                StartDate=setupEntity.StartDate,
                EndDate=setupEntity.EndDate,
                Id=setupEntity.Id,
                AssessmentName=setupEntity.AssessmentName
                
            };
            return ent;
        }
        public List<EnumViewModel> GetQuestionCate(int Id)
        {
            var entity = new List<EnumViewModel>();
            var setupEntity = _accessmentRepository.GetSingleRecordFilter(x => x.Id == Id);
          var getQuestCat=  setupEntity.QuestionCategories.Split(',');
            foreach(var q in getQuestCat)
            {
                var value = int.Parse(q);
              var record=  _questionRepository.GetSingleRecordFilter(x => x.Id == value && !x.IsDeleted);
                entity.Add(new EnumViewModel { Value=record.Id,
                Name=record.CategoryName}
                    );
            }




            return entity;
        }


        public IList<AssessmentSetupVM> GetAllDataTable(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount)
        {
            var searchBy = (model.search != null) ? model.search.value : null;
            var take = model.length;
            var skip = model.start;
            string sortBy = "";
            bool sortDir = true;

            if (model.order != null)
            {
                // in this example we just default sort on the 1st column
                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == "asc";
            }
            // search the dbase taking into consideration table sorting and paging
            var result = GetDataFromDbase(searchBy, take, skip, sortBy, sortDir, out filteredResultsCount, out totalResultsCount);
            if (result == null)
            {
                result = new List<AssessmentSetupVM>();
                return result;
            }
            return result;
        }
        private List<AssessmentSetupVM> GetDataFromDbase(string searchBy, int take, int skip, string sortBy, bool sortDir, out int filteredResultsCount, out int totalResultsCount)
        {
            //var role = Enum.Parse(typeof(UserRoleEnum), searchBy);
            var record = _accessmentRepository.GetAllIncluding(x => !x.IsDeleted, false);
            totalResultsCount = record.Count();
            if (!String.IsNullOrEmpty(searchBy))
                record = record.Where(x => x.AssessmentName.ToLower().Contains(searchBy.ToLower()));
            //SortBy
            record = sortBy == "AssessmentName" ? record.OrderBy<AssessmentSetup>(x => x.AssessmentName, sortDir) :
                  record.OrderByDescending(x => x.Id);
            filteredResultsCount = record.Count();
            var result = record.Skip(skip).Take(take).ToList();
            return result.Select(x => new AssessmentSetupVM
            {
                AssessmentName = x.AssessmentName,
                Id = x.Id,
                StartDate=x.StartDate,
                EndDate=x.EndDate
            }).ToList();
        }

        public async Task<bool> DeleteAssessment(AssessmentSetupVM model)
        {
            try
            {
                var setupEntity = _accessmentRepository.GetAllIncluding(x => !x.IsDeleted && x.Id == model.Id, true).FirstOrDefault();
                _accessmentRepository.Delete(setupEntity);
                var check = await _accessmentRepository.SaveAsync();
                return check == 1;
            }
            catch (Exception ex)
            {
                _loggerInfo.Error(ex, "Setup Assessment (Delete)");
                return false;
            }
        }
    }
}
