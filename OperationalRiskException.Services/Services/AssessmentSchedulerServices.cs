﻿using NLog;
using OperationalRiskException.Core.AuthVM;
using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.Enums;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Data.GenericRepository.IRepository;
using OperationalRiskException.Data.GenericRepository.Repository;
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Services.Services
{
    public class AssessmentSchedulerServices : IAssessmentSchedulerServices
    {
        private IRepository<AssessmentScheduling> _assessSchRepo;
        private IRepository<AssessmentSetup> _assessSetupRepo;
        private IRepository<User> _userRepo;

        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();
        public AssessmentSchedulerServices()
        {
            _assessSchRepo = new Repository<AssessmentScheduling>();
            _assessSetupRepo = new Repository<AssessmentSetup>();
            _userRepo = new Repository<User>();
        }
        public AssessmentSetup GetAssessmentSetup(int Id)
        {
            var item = _assessSetupRepo.GetSingleRecordFilter(x => x.Id == Id && !x.IsDeleted);
            return item;
        }
        public async Task<bool> CreateAddUser(AssessmentSchedulerVM model)
        {
            try
            {
                int countSuccess = 0;
                int countExisted = 0;
                var assessmentId = int.Parse(model.AssessmentSetupId);
                foreach (var usr in model.ListOfUsers)
                {
                    var userId = int.Parse(usr);
                    var entity = new AssessmentScheduling
                    {
                        AssessmentSetupId=assessmentId,
                        UserId=userId,
                        AssessmentSchedulerName=model.AssessmentSchedulerName,
                       
                    };
                    var checker =await _assessSchRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted && x.AssessmentSetupId == assessmentId && x.UserId == userId);
                    if (checker == null)
                    {
                        _assessSchRepo.Insert(entity);
                       // countSuccess+=_assessSchRepo.Save();
                    }
                  
                }
                _loggerInfo.Info( "Assessment Schedule  (Create)::: NEW Records count:-" +countSuccess + " Existing Record(s):::"+countExisted);
               countSuccess= _assessSchRepo.Save();

                return countSuccess > 0 ;
            }
            catch (Exception ex)
            {
                _loggerInfo.Error(ex, "Assessment Schedule  (Create)");

                return false;
            }
        }
        public async Task<bool> CreateUpdate(AssessmentSchedulerVM model)
        {
            try
            {
                var entity = _assessSchRepo.GetAllIncluding(x => x.AssessmentSetupId == model.Id && !x.IsDeleted, true);
                foreach(var item in entity)
                {
                    _assessSchRepo.Delete(item);
                }
                if (entity != null)
                    _assessSchRepo.Save();
                return await CreateAddUser(model);
             }
            catch (Exception ex)
            {
                _loggerInfo.Error(ex, "Assessment Schedule  (Update)");
                return false;
            }
        }


        public List<AssessmentSetupVM> GetAssessmentSchUsers()
        {
            var assEntit = new List<AssessmentSetupVM>();
            var entySetup = _assessSchRepo.GetAllIncluding(x => !x.IsDeleted, false, x => x.User, y=>y.AssessmentSetup).GroupBy(x=>new {  x.AssessmentSetupId});
            var assEntity = entySetup.Select(x => new AssessmentSetupVM
            {
                Id = x.Key.AssessmentSetupId,
                StartDate = x.Select(y=>y.AssessmentSetup.StartDate).FirstOrDefault(),
                EndDate = x.Select(y => y.AssessmentSetup.EndDate).FirstOrDefault(),
                Users = x.Select(y => new AssessmentUserSetup
                {
                    Department = y.User.DepartmentName,
                    Username = y.User.Username,
                    UserId = y.UserId
                })
                //  Users = x.Users.ToList();
            });

            return assEntity.ToList() ?? assEntit;

        }


        public IList<AssessmentSetupVM> GetAllDataTable(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount)
        {
            var searchBy = (model.search != null) ? model.search.value : null;
            var take = model.length;
            var skip = model.start;
            string sortBy = "";
            bool sortDir = true;

            if (model.order != null)
            {
                // in this example we just default sort on the 1st column
                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == "asc";
            }
            // search the dbase taking into consideration table sorting and paging
            var result = GetDataFromDbase(searchBy, take, skip, sortBy, sortDir, out filteredResultsCount, out totalResultsCount);
            if (result == null)
            {
                result = new List<AssessmentSetupVM>();
                return result;
            }
            return result;
        }
        private List<AssessmentSetupVM> GetDataFromDbase(string searchBy, int take, int skip, string sortBy, bool sortDir, out int filteredResultsCount, out int totalResultsCount)
        {
            //var role = Enum.Parse(typeof(UserRoleEnum), searchBy);
            var assEntit = new List<AssessmentSetupVM>();
            var record = _assessSchRepo.GetAllIncluding(x => !x.IsDeleted, false, x => x.User, y => y.AssessmentSetup).GroupBy(x => new { x.AssessmentSetupId });
          var  assEntity = record.Select(x => new AssessmentSetupVM
            {
                Id = x.Key.AssessmentSetupId,
                AssessmentName = x.Select(y => y.AssessmentSetup.AssessmentName).FirstOrDefault(),
                StartDate = x.Select(y => y.AssessmentSetup.StartDate).FirstOrDefault(),
                EndDate = x.Select(y => y.AssessmentSetup.EndDate).FirstOrDefault(),
                Users = x.Select(y => new AssessmentUserSetup
                {
                    Department = y.User.DepartmentName,
                    Username = y.User.Username,
                    UserId = y.UserId
                })

            });
            if (!string.IsNullOrEmpty(searchBy))
            {
                assEntity= assEntity.Where(x => x.AssessmentName.Contains( searchBy) );
            }
            totalResultsCount = assEntity.Count();
            assEntity = sortBy == "AssessmentName" ? assEntity.OrderBy<AssessmentSetupVM>(x => x.AssessmentName, sortDir) :
                  assEntity.OrderByDescending(x => x.Id);
            filteredResultsCount = assEntity.Count();
            var result = assEntity.Skip(skip).Take(take).ToList();
            return assEntity.ToList() ?? assEntit ;
        }


        public List<EnumViewModel> GetAllUserForAssessment(int AssId)
        {
            //var role = Enum.Parse(typeof(UserRoleEnum), searchBy);
            var entity = new List<EnumViewModel>();
            var record = _assessSchRepo.GetAllIncluding(x => !x.IsDeleted && x.AssessmentSetupId==AssId, false, x => x.User);
            foreach(var item in record)
                entity.Add( new EnumViewModel { Value = item.UserId, Name = item.User.Username });
          
            return entity;
        }

        public Select2PagedResult GetAllAssessmentPeriod(string searchTerm, int pageSize, int pageNumber)
        {
            var select2pagedResult = new Select2PagedResult
            {
                Results = GetPagedListAssessment(searchTerm, pageSize, pageNumber, out int totalResults),
                Total = totalResults
            };
            return select2pagedResult;
        }

        private List<Select2OptionModel> GetPagedListAssessment(string searchTerm, int pageSize, int pageNumber, out int totalResults)
        {
            var record = _assessSetupRepo.GetAllIncluding(x => !x.IsDeleted, false);
            if (!String.IsNullOrEmpty(searchTerm))
                record = record.Where(x => x.AssessmentName.ToLower().Contains(searchTerm.ToLower()));
            totalResults = record.Count();
            record = record.OrderBy(x => x.AssessmentName);
            var result = record.Skip(pageNumber - 1).Take(pageSize).ToList();
            return result.Select(x => new Select2OptionModel
            {
                id = x.Id.ToString(),
                text = x.AssessmentName
            }).ToList();

        }
        public Select2PagedResult GetAllUsers(string searchTerm, int pageSize, int pageNumber)
        {
            var select2pagedResult = new Select2PagedResult
            {
                Results = GetPagedListOptionsUsers(searchTerm, pageSize, pageNumber, out int totalResults),
                Total = totalResults
            };
            return select2pagedResult;
        }

        private List<Select2OptionModel> GetPagedListOptionsUsers(string searchTerm, int pageSize, int pageNumber, out int totalResults)
        {
            var record = _userRepo.GetAllIncluding(x => !x.IsDeleted && x.UserRole==UserRoleEnum.Champion, false);
            if (!String.IsNullOrEmpty(searchTerm))
                record = record.Where(x => x.Username.ToLower().Contains(searchTerm.ToLower()));
            totalResults = record.Count();
            record = record.OrderBy(x => x.Username);
            var result = record.Skip(pageNumber - 1).Take(pageSize).ToList();
            return result.Select(x => new Select2OptionModel
            {
                id = x.Id.ToString(),
                text = x.Username
            }).ToList();

        }


    }
}
