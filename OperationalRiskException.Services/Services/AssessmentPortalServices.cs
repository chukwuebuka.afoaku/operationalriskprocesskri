﻿using NLog;
using OperationalRiskException.Core.Dtos;
using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.Enums;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Data.GenericRepository.IRepository;
using OperationalRiskException.Data.GenericRepository.Repository;
using OperationalRiskException.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Services.Services
{
    public class AssessmentPortalServices : IAssessmentPortalServices
    {
        private IRepository<AssessmentScheduling> _assessmentScheduler;
        private IRepository<AssessmentSetup> _assessmentSetup;
        private IRepository<User> _userRepo;
        private IRepository<Question> _questionRepo;
        private IRepository<QuestionCategory> _questionCategoryRepo;
        private IRepository<AssessmentRecord> _assessmentRecRepo;

        private IRepository<AnswerEntity> _answerRepo;
        private IRepository<CorrectivePlanSetup> _correctiveSetupRepo;

        private IRepository<CorrectivePlan> _correctivePlanRepo;
        private IRepository<SaveAssessmentByCategory> _saveAssessmentByCategory;


        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();
        public AssessmentPortalServices()
        {
            _assessmentScheduler = new Repository<AssessmentScheduling>();
            _userRepo= new Repository<User>();
            _assessmentSetup= new Repository<AssessmentSetup>();
            _questionRepo = new Repository<Question>();
            _questionCategoryRepo = new Repository<QuestionCategory>();
            _assessmentRecRepo=new Repository<AssessmentRecord>();
            _answerRepo= new Repository<AnswerEntity>();
            _correctiveSetupRepo = new Repository<CorrectivePlanSetup>();
            _correctivePlanRepo = new Repository<CorrectivePlan>();
            _saveAssessmentByCategory = new Repository<SaveAssessmentByCategory>();

        }
        public async Task<ChampionDashboardVM> GetAllRecords(string username)
        {
            var entity = new ChampionDashboardVM();
            var user =await  _userRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted && x.Username.ToLower() == username);
            entity.UserId = user.Id;
            entity.Username = username;
            //
            var checkAVailableTest = _assessmentScheduler.GetAllIncluding(x => !x.IsDeleted && x.UserId == entity.UserId  && !x.IsAnswered, false);
            var presentDate =DateTime.Now.Date.ToString("yyyy-MM-dd");
           // var assessmentList =  _assessmentSetup.GetWithRawSql($"select * from AssessmentSetupsWHERE '{presentDate}' between StartDate AND EndDate   ").Where(x => checkAVailableTest.Select(y=>y.AssessmentSetupId).Contains(x.Id));
            var assessmentList =  _assessmentSetup.GetWithRawSql($"select * from AssessmentSetups").Where(x => checkAVailableTest.Select(y=>y.AssessmentSetupId).Contains(x.Id));
            //result of test
            entity.IsTestAvailable = assessmentList != null ? true : false;
            entity.ListOfAvailableTest = assessmentList.Select(x => new AvailableAssessment {AssessmentName=x.AssessmentName, AssessmentScheduleId=x.Id }).ToList();
            entity.Scores = GetAllScoreUser(username);
            return entity;
        }


        public bool IsAssessmentValid( string userId, int SchId)
        {

            var checker = _assessmentScheduler.GetAllIncluding(x => x.User.Username.ToLower() == userId.ToLower() && x.AssessmentSetupId == SchId 
            && !x.IsAnswered, false, y=>y.User).FirstOrDefault();
            return checker != null;

        }

        public async Task<List<QuestionMgtVM>> GetAllRecordAllQuestions(string username, int AssSchId)
        {
            try
            {
                //  QuestionCategories
                var questnMgt = new List<QuestionMgtVM>();
                var user = await _userRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted && x.Username.ToLower() == username);
                if (user == null)
                    throw new Exception("User doesn't exist on the platform");

                var checkScheduler =  _assessmentScheduler.GetSingleRecordFilter(x =>  !x.IsDeleted && !x.IsAnswered && x.UserId==user.Id && x.AssessmentSetupId == AssSchId);
                if (checkScheduler == null)
                    throw new Exception("There is no test scheduled for the user");
                // Check Date range whether is between the range; Today
                var presentDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                //var assessmentList = _assessmentSetup.GetWithRawSql($"select * from AssessmentSetups WHERE '{presentDate}' between StartDate AND EndDate").Where(x=>x.Id==AssSchId).FirstOrDefault();
                var assessmentList = _assessmentSetup.GetWithRawSql($"select * from AssessmentSetups").Where(x=>x.Id==AssSchId).FirstOrDefault();
                if (assessmentList == null)
                    throw new Exception("There is no avaliable");
                var allQuestionCategories = assessmentList.QuestionCategories.Split(',');
                foreach(var questCat in allQuestionCategories)
                {
                    var qc = int.Parse(questCat);
                    var qcDetails= _questionCategoryRepo.GetSingleRecordFilter(x => x.Id == qc);
                    var questDetails = _questionRepo.GetAllIncluding(x => x.QuestionCategoryId == qc && !x.IsDeleted && x.ApprovalStatus==ApprovalStatus.Approved, false);

                    var allQuestionCategory = new List<TestQuestionVM>();
                    foreach (var questn in questDetails)
                    {
                        allQuestionCategory.Add(new TestQuestionVM
                        {
                            CategoryName = qcDetails.CategoryName,
                            CategoryId = qcDetails.Id,
                            QuestionId=questn.Id,
                            QuestionText=questn.QuestionText,
                            AnswerTextA=questn.AnswerTextA,
                            AnswerTextB = questn.AnswerTextB,
                            AnswerTextC = questn.AnswerTextC,
                            AnswerTextD = questn.AnswerTextD,
                            QuestionType = questn.QuestionType,






                        });
                    }
                    questnMgt.Add(new QuestionMgtVM
                    {
                        CategoryName = qcDetails.CategoryName,
                        QuestionCategoryId = qcDetails.Id,
                        TestQuestion = allQuestionCategory,
                        Count= questDetails.Count()

                    }) ;
                    
                }

                return questnMgt;
            }
            catch(Exception ex)
            {
                _loggerInfo.Error(ex, "AssessmentPortal Get Question for Users:::::");
                return new List<QuestionMgtVM>();
            }


        }
        private int SaveAssessmentRecord(int userId, int SetUpId, int QuestionCategoryId) {
            var saveEntity = new AssessmentRecord()
            {
                QuestionCategoryId = QuestionCategoryId,

                AssessmentSetupId = SetUpId,
                UserId = userId
            };
            _assessmentRecRepo.Insert(saveEntity);
            var checkSave = _assessmentRecRepo.Save() == 1;
            _loggerInfo.Info("SaveAssessmentRecord: check::  "+checkSave.ToString());
            return saveEntity.Id;
        }
        public async Task<bool> SaveAssessment(SaveQuestionVM questionVM, string username,  int SetUpId)
        {
            try
            {
                // File Upload before saving answer (Flow)
                
                var user = await _userRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted && x.Username.ToLower() == username.ToLower());
                if (user == null)
                    throw new Exception("User doesn't exist");
                var checkAnswer = await GetAssessmentRecord(questionVM.QuestionCategoryId, user.Id, SetUpId);
                int ansrealId = checkAnswer == null ? SaveAssessmentRecord(user.Id, SetUpId, questionVM.QuestionCategoryId) : checkAnswer.Id;

                //throw new Exception("File doesn't exist ");
                //Delete all the previous answers
                int value = 0;
               
                  
                var allAnswers = _answerRepo.GetAllIncluding(x => x.AssessmentRecordId == ansrealId && !x.IsDeleted , true);
                foreach(var item in allAnswers.ToList())
                {
                    //Update records
                    _answerRepo.Delete(item);
                    _answerRepo.Save();
                }
                foreach (var questn in questionVM.Questionlists) {
                    var questnNO = int.Parse(questn.name);
                    var _questionDetails = _questionRepo.GetAllIncluding(x => x.Id == questnNO, false).FirstOrDefault();
                    var ans = new AnswerEntity();
                    
                    ans.QuestionType = _questionDetails.QuestionType;
                    ans.AssessmentRecordId = ansrealId;
                    ans.QuestionId = questnNO;
                    value = int.Parse(questn.value);

                    if (_questionDetails.QuestionType == QuestionTypeEnum.Numerical)
                    {
                        // var answer = (AnswerEnum)int.Parse(questn.value);
                      
                        var NumericAnswer = _questionDetails.NumericAnswer;

                        if (NumericAnswer >= value)
                        {
                            ans.IsCorrectAnswer = true;
                        }
                        else
                        {
                            ans.IsCorrectAnswer = false;
                        }
                      



                    }
                    else if (_questionDetails.QuestionType == QuestionTypeEnum.Options)
                    {
                        var selectedAnswer = value;
                        int choice = (int)_questionDetails.ChoiceAnswer;
                        var rightAnswer=choice == selectedAnswer;
                        if (rightAnswer)
                        {
                            ans.IsCorrectAnswer = true;
                        }
                        else
                        {
                            ans.IsCorrectAnswer = false;
                        }
                        ans.answerEnum = (AnswerEnum)selectedAnswer;
                    }
                    else if(_questionDetails.QuestionType == QuestionTypeEnum.YesNoType)
                    {
                        var selectedAnswer = value;
                        var rightAnswer = 1 == selectedAnswer;
                        if (rightAnswer)
                        {
                            ans.IsCorrectAnswer = true;
                        }
                        else
                        {
                            ans.IsCorrectAnswer = false;
                        }
                        ans.answerEnum = (AnswerEnum)selectedAnswer;
                    }
                    ans.AnswerNumber = (int)value;
                     _answerRepo.Insert(ans);
                    _answerRepo.Save();
                }
                if (questionVM.IsCompleted)
                {
                    var userAnswers =  GetAssessmentRecords( user.Id, SetUpId);
                    UpdateAssessment(user.Id, userAnswers, SetUpId);
                    var checkScheduler = _assessmentScheduler.GetSingleRecordFilter(x => !x.IsDeleted && !x.IsAnswered && x.UserId == user.Id  &&x.AssessmentSetupId== SetUpId);
                    if (checkScheduler != null)
                    {
                        checkScheduler.IsAnswered = true;
                        _assessmentScheduler.Update(checkScheduler);
                        _assessmentScheduler.Save();
                    }
                }

                    return true;

            }
            catch (Exception ex)
            {
                _loggerInfo.Error(ex, "AssessmentPortal Answer Users::");
                return false;
            }
        }
        public async Task<MainAnswerDto> GetAllSavedAssessment(string username, int QuestionCategoryId, int SetUpId)
        {
            var list = new MainAnswerDto();
            try
            {
                var user = await _userRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted && x.Username.ToLower() == username.ToLower());
                if (user == null)
                    throw new Exception("User doesn't exist");
                var checkAnswer = await GetAssessmentRecord(QuestionCategoryId, user.Id, SetUpId);
                if (checkAnswer == null)
                    throw new Exception("File doesn't exist ");
                //Delete all the previous answers
                var allAnswers = _answerRepo.GetAllIncluding(x => x.AssessmentRecordId == checkAnswer.Id && !x.IsDeleted, true);


                list.answers = allAnswers.Select(x => new AnswersDto
                {
                    answerEnum = x.answerEnum,
                    AnswerValue =(int)x.AnswerNumber,
                    QuestionId = x.QuestionId,
                    QuestionType=x.QuestionType
                }).ToList();
                return list;
            }
            catch (Exception ex)
            {
                _loggerInfo.Error(ex.ToString(), "AssessmentPortal GetAllAssessment::");
                return list;
            }
        }

        private void UpdateAssessment(int userId,List<AssessmentRecord> userAnswers, int SetUpId)
        {
            foreach (var scores in userAnswers)
            {
                var assessmentCount = _answerRepo.GetAllIncluding(x => x.AssessmentRecordId == scores.Id && !x.IsDeleted, true).ToList();

                var category = new SaveAssessmentByCategory();

                category.CategoryId = scores.QuestionCategoryId;
                category.UserId = userId;
                category.TotalQuestion = assessmentCount.Count;
                category.AssessmentSetupId = SetUpId;
                category.CategoryScore = assessmentCount.Where(x => !x.IsCorrectAnswer).Count();
                _saveAssessmentByCategory.Insert(category);
                _saveAssessmentByCategory.Save();



            }
        }
        public List<MainAnswerDto> GetAllSavedAssessments(int userId, int SetUpId)
        {
            var list = new List< MainAnswerDto>();
            try
            {
              
                var checkAnswer =  GetAssessmentRecords(userId, SetUpId);
                if (checkAnswer == null)
                    throw new Exception(" doesn't exist ");

                foreach(var item in checkAnswer)
                {
                    var allAnswers = _answerRepo.GetAllIncluding(x => x.AssessmentRecordId == item.Id && !x.IsDeleted, false,y=>y.Question ).ToList();
                    var mainRecord = new MainAnswerDto();
                    mainRecord.FileDto = item.FilePath;
                    mainRecord.CategoryName = item.QuestionCategory.CategoryName;
                    mainRecord.CategoryId = item.QuestionCategoryId;
                    mainRecord.answers = allAnswers.Select(x => new AnswersDto
                    {
                        answerEnum = x.answerEnum,
                        QuestionId = x.QuestionId,
                        QuestionTest = x.Question.QuestionText,
                        AnswerTextA = x.Question.AnswerTextA,
                        AnswerTextB = x.Question.AnswerTextB,
                        AnswerTextC = x.Question.AnswerTextC,
                        AnswerTextD = x.Question.AnswerTextD,
                        QuestionType =x.QuestionType,
                        AnswerValue=(int)x.AnswerNumber
                    }).ToList();

                     list.Add(mainRecord);
                }
                return list;
            }
            catch (Exception ex)
            {
                _loggerInfo.Error(ex, "AssessmentPortal GetAllAssessment::");
                return list;
            }
        }

        public async Task<bool> SaveFile(FileUploadVM questionVM, string username, int SetUpId)
        {
            try
            {
                var user = await _userRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted && x.Username.ToLower() == username.ToLower());
                if (user == null)
                    throw new Exception("User doesn't exist");
                var checkAnswer = await GetAssessmentRecord(questionVM.QuestionCategoryId, user.Id, SetUpId);
                if (checkAnswer == null)
                {
                    var saveEntity = new AssessmentRecord()
                    {
                        QuestionCategoryId=questionVM.QuestionCategoryId,
                        FilePath=questionVM.FilePath,
                        AssessmentSetupId= SetUpId,
                        UserId=user.Id
                    };
                    _assessmentRecRepo.Insert(saveEntity);
                    return _assessmentRecRepo.Save()==1;
                   
                }
                else
                {
                    checkAnswer.FilePath = questionVM.FilePath;
                    _assessmentRecRepo.Update(checkAnswer);
                    return _assessmentRecRepo.Save() == 1;
                }

            }
            catch (Exception ex)
            {
                _loggerInfo.Error(ex, "AssessmentPortal FileUpload for Users:::::");
                return false;
            }
        }

        public List<ScoreVM> GetAllScoreUser(string username)
        {
            var user =  _userRepo.GetSingleRecordFilter(x => !x.IsDeleted && x.Username.ToLower() == username.ToLower());
            if (user == null)
                throw new Exception("User doesn't exist");
            var getRecords = _assessmentScheduler.GetAllIncluding(x => x.IsAnswered && !x.IsDeleted, false, y => y.User);
            getRecords = getRecords.Where(x => x.UserId == user.Id);
            var passScheduler = getRecords.ToList();
            return GetScores(passScheduler);
        }
        //public async Task<bool> UserCheck(string username, int AsschId)
        //{
        //    var user =await  _userRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted && x.Username.ToLower() == username.ToLower());
        //    if (user == null)
        //        throw new Exception("User doesn't exist");

        //}
        public List<ScoreVM> GetAllScores()
        {
            var listScore = new List<ScoreVM>();
            var getRecords = _assessmentScheduler.GetAllIncluding(x => x.IsAnswered && !x.IsDeleted, false, y=>y.User).ToList();
            return GetScores(getRecords);
        }
        private List<ScoreVM> GetScores(List<AssessmentScheduling> getRecords)
        {
            var listScore = new List<ScoreVM>();
            foreach (var item in getRecords)
            {
                // //Select All the Main assessment record with the userid and AssessmentSetupId
                var allAssessmentRecords = _assessmentRecRepo.GetAllIncluding(x => x.AssessmentSetupId == item.AssessmentSetupId &&x.UserId==item.UserId && !x.IsDeleted, false).GroupBy(x => new {
                    x.UserId,
                    x.AssessmentSetupId
                }).ToList();
                int userScoreYes = 0;
                int userScoreNo = 0;
                int totalQuestn = 0;
                string Username = item.User.Username;
                string Department = item.User.DepartmentName;
                int assessmentsetupId = item.AssessmentSetupId;
                int userId = item.UserId;
                //
               // var userAnswers = GetAssessmentRecords(userId, item.AssessmentSetupId);
               // UpdateAssessment(userId, userAnswers);
                foreach (var record in allAssessmentRecords)
                {
                    //Select User loop singleUser
                    

                    var listass = record.Select(x => x.Id);

                    foreach (var assitem in listass)
                    {
                        //All answers
                        var ansCon = _answerRepo.GetAllIncluding(x => x.AssessmentRecordId == assitem && !x.IsDeleted, false).ToList();
                        totalQuestn += ansCon.Count();
                        // userScoreYes += ansCon.Where(x => x.answerEnum == AnswerEnum.Yes).Count();
                        userScoreNo += ansCon.Where(x => !x.IsCorrectAnswer).Count();
                    }


                }
                double NumberNo = userScoreNo * 5;
                double QuestNo = totalQuestn * 5;
                double Risk = NumberNo / QuestNo * 100;

                var usser = new ScoreVM
                {
                    TotalScore = totalQuestn,
                    ScoreYes = userScoreYes,
                    Department = Department,
                    Username = Username,
                    ScoreNo = userScoreNo,
                    Risk = Risk,
                    AssessmentSetupId = assessmentsetupId,
                    UserId = userId

                };
                listScore.Add(usser);

            }
            return listScore;
        }


        public List<TestQuestionVM> GetCorrectiveFormByUsername(int schedulerId, string username)
        {

            var allQuestionCategory = new List<TestQuestionVM>();
            try
            {
                var checkScheduler = _assessmentScheduler.GetAllIncluding(x => x.IsAnswered && !x.IsDeleted && x.AssessmentSetupId == schedulerId && !x.HasCorrectivePlan, false, y => y.User).FirstOrDefault();
                if (checkScheduler == null)
                    throw new Exception("Scheduler Check = null");
                var user = _userRepo.GetSingleRecordFilter(x => !x.IsDeleted && x.Username.ToLower() == username.ToLower());
                if (user == null)
                    throw new Exception("User doesn't exist");
                var allAssessmentRecords = _assessmentRecRepo.GetAllIncluding(x => x.AssessmentSetupId == checkScheduler.AssessmentSetupId && x.UserId==user.Id  && !x.IsDeleted, false).GroupBy(x => new {
                    x.UserId,
                    x.AssessmentSetupId
                }).Select(x => x.Select(y=>y.Id)).ToList();

                var listass = allAssessmentRecords.SelectMany(x => x);

                foreach (var assitem in listass)
                {
                    //All answers
                  
                    var ansCon = _answerRepo.GetAllIncluding(x => x.AssessmentRecordId == assitem && !x.IsDeleted &&!x.IsCorrectAnswer, false, y=>y.Question).ToList();
               
                    foreach (var item in ansCon)
                    {
                    
                            //Bind 
                            allQuestionCategory.Add(new TestQuestionVM
                            {
                                QuestionId = item.QuestionId,
                                QuestionText = item.Question.QuestionText,
                            });

                    
                    }
                   
                }



                return allQuestionCategory;

            }
            catch (Exception ex)
            {
               return allQuestionCategory;

            }
          
        }

        public async Task<bool> SaveCorrectivePlan(SaveQuestionVM questionVM, string username, int SetUpId)
        {
            int correctiveSetupId = 0;
            try
            {
                // File Upload before saving answer (Flow)

                var user = await _userRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted && x.Username.ToLower() == username.ToLower());
                if (user == null)
                    throw new Exception("User doesn't exist");

                var checkCorrective = _correctiveSetupRepo.GetAllIncluding(x => x.AssessmentSetupId == SetUpId && x.UserId == user.Id, false).FirstOrDefault();
              if(checkCorrective != null)
                    throw new Exception("Corrective Plan  existed before");
                var correctiveSetup = new CorrectivePlanSetup
                {
                    AssessmentSetupId=SetUpId,
                    UserId=user.Id,
                    

                };
                _correctiveSetupRepo.Insert(correctiveSetup);
                var check=_correctiveSetupRepo.Save();
                if (check == 1)
                {
                    correctiveSetupId = correctiveSetup.Id;
                    if (correctiveSetupId > 0)
                    {
                        foreach (var questn in questionVM.Questionlists)
                        {
                            var answer = questn.value;
                            var questnNO = int.Parse(questn.name);
                            _correctivePlanRepo.Insert(new CorrectivePlan
                            {
                                CorrectiveText = answer,
                                QuestionId = questnNO,
                                CorrectivePlanSetupId = correctiveSetupId
                            });
                            _correctivePlanRepo.Save();
                        }
                    }
                }

                    var checkScheduler = _assessmentScheduler.GetSingleRecordFilter(x => !x.IsDeleted && !x.HasCorrectivePlan && x.UserId == user.Id &&  x.AssessmentSetupId == SetUpId);
                    if (checkScheduler != null)
                    {
                        checkScheduler.HasCorrectivePlan = true;
                        _assessmentScheduler.Update(checkScheduler);
                        _assessmentScheduler.Save();
                    }
                

                return true;

            }
            catch (Exception ex)
            {
                _loggerInfo.Error(ex, "AssessmentPortal CorrectivePlan Saves::");
                return false;
            }
        }
        public List<CorrectivePlanDisplayVM> getAllCorrectivePlan( int userId, int SetUpId)
        {
            var lists = new List<CorrectivePlanDisplayVM>();
            try
            {
                // File Upload before saving answer (Flow)

                var checkCorrective = _correctiveSetupRepo.GetAllIncluding(x => x.AssessmentSetupId == SetUpId && x.UserId == userId, false).FirstOrDefault();
                if (checkCorrective == null)
                    throw new Exception("Corrective Plan  not exist ");
                var getCorrectivePlans = _correctivePlanRepo.GetAllIncluding(x => x.CorrectivePlanSetupId == checkCorrective.Id, false, y=>y.Question).ToList();
                var results = getCorrectivePlans.Select(x => new CorrectivePlanDisplayVM {
                    CorrectivePlan=x.CorrectiveText,
                    QuestionTest=x.Question.QuestionText
                }).ToList();
                lists = results;
                return lists;


            }
            catch (Exception ex)
            {
                _loggerInfo.Error(ex, "AssessmentPortal CorrectivePlan Display::");
                return lists;
            }
        }
        private async Task<AssessmentRecord> GetAssessmentRecord(int questCatId, int userId, int SetUpId)
        {
         var record=   await _assessmentRecRepo.GetSingleRecordFilterAsync(x => !x.IsDeleted
                  && x.QuestionCategoryId == questCatId && x.UserId == userId && x.AssessmentSetupId==SetUpId);
            return record;
        }
        private  List<AssessmentRecord> GetAssessmentRecords( int userId, int SetUpId)
        {
            var record =  _assessmentRecRepo.GetAllIncluding(x => !x.IsDeleted
                     && x.UserId == userId && x.AssessmentSetupId == SetUpId, false, y=>y.QuestionCategory);
            return record.ToList();
        }

    }
}
