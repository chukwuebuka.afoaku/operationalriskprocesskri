﻿using OperationalRiskException.Core.AuthVM;
using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Services.Interfaces
{
    public interface IAssessmentSetupService
    {

        Task<bool> CreateAssessment(AssessmentSetupVM assessmentVM);
        Select2PagedResult GetAllQuestionCategories(string searchTerm, int pageSize, int pageNumber);
        Task<bool> UpdateAssessment(AssessmentSetupVM assessmentVM);
        Task<bool> Delete(AssessmentSetupVM assessmentVM);
        AssessmentSetupVM GetAssessmentSetup(int id);
        IList<AssessmentSetupVM> GetAllDataTable(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount);
        List<EnumViewModel> GetQuestionCate(int Id);
        Task<bool> DeleteAssessment(AssessmentSetupVM model);
    }
}
