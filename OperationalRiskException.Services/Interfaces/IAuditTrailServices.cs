﻿using OperationalRiskException.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Services.Interfaces
{
  public  interface IAuditTrailServices
    {
       void AddAudit(AuditTrail model);
    }
}
