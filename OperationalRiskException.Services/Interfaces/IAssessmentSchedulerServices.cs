﻿using OperationalRiskException.Core.AuthVM;
using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Services.Interfaces
{
    public interface IAssessmentSchedulerServices
    {
       // AssessmentSchedulerVM CreateAddUser();
        Task<bool> CreateAddUser(AssessmentSchedulerVM model);
        Task<bool> CreateUpdate(AssessmentSchedulerVM model);
        List<AssessmentSetupVM> GetAssessmentSchUsers();
        Select2PagedResult GetAllAssessmentPeriod(string searchTerm, int pageSize, int pageNumber);
        Select2PagedResult GetAllUsers(string searchTerm, int pageSize, int pageNumber);
        IList<AssessmentSetupVM> GetAllDataTable(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount);
        List<EnumViewModel> GetAllUserForAssessment(int AssId);
        AssessmentSetup GetAssessmentSetup(int Id);
    }
}
