﻿using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Core.Dtos;
using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Services.Interfaces
{
    public interface IDashboardService
    {
        DashboardDto DashBoard();
    }
}
