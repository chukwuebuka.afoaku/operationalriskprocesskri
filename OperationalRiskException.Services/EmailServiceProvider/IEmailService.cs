﻿using OperationalRiskException.Core.Dtos;

namespace OperationalRiskException.Services.EmailServiceProvider
{
    public interface IEmailService
    {
        bool EscalationEmail(string supervUser, Core.Dtos.ChampionEscalationDTO pendingItem);
        bool SoftWarning(string supervUser, ChampionEscalationDTO pendingItem);
    }
}