﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Enums
{
    public enum AnswerEnum
    {
        NothingSelected = 0,
       [Description("A or Yes")]
        A =1,
        [Description("B or No")]
        B =2,
        [Description("C or Not Applicable")]
        C =3,
        [Description("D or Not Applicable")]
        D = 4,


    }
}
