﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
  public  class ScriptVM
    {
       public int UserId { get; set; }
        public int SetupId { get; set; }
        public List<CorrectivePlanDisplayVM> Plans { get; set; }
    }
}
