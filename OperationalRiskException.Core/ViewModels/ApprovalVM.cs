﻿using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
   public class ApprovalVM
    {
        public int QuestionId { get; set; }

        public ApprovalStatus ApprovalStatus { get; set; }
        public string Username { get; set; }
    }
}
