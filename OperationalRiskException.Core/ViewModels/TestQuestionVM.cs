﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OperationalRiskException.Core.Enums;

namespace OperationalRiskException.Core.ViewModels
{
    public class TestQuestionVM
    {
        public int QuestionId { get; set; }
        public string QuestionText { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string AnswerTextC { get; set; }
        public string AnswerTextA { get; set; }
        public string AnswerTextB { get; set; }
        public string AnswerTextD { get; set; }
        public QuestionTypeEnum QuestionType { get; set; }
        public string QuestionTypeText => QuestionType.ToString();
    }
}
