﻿using OperationalRiskException.Core.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
    public class AssessmentSchedulerVM : BaseVM
    {
        public int Id { get; set; }
      
        public string AssessmentSchedulerName { get; set; }
        [Required(ErrorMessage = "You must select at least one user")]
        [RequiredArray(ErrorMessage = "You must select at least one user")]
        public string[] ListOfUsers { get; set; }
        [Required]
        public string AssessmentSetupId{ get; set; }


    }
}
