﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
    public class ReportDisplays
    {
        // Reports myDeserializedClass = JsonConvert.DeserializeObject<Reports>(myJsonResponse); 

        public class header
        {
            public string key { get; set; }
            public string value { get; set; }
        }

        public class body
        {
            public string key { get; set; }
            public string value { get; set; }
        }

        public class ReportResponce
        {
            public List<header> tableHeader { get; set; }
            public List<List<body>> tableValue { get; set; }
        }

        public class ReportRequest
        {
            public string Category { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
        }

    }
}
