﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
    public class CorrectivePlanDisplayVM
    {
        public string QuestionTest { get; set; }
        public string CorrectivePlan { get; set; }
    }
}
