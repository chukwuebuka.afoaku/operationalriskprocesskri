﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
 public    class RoleViewModel
    {
        public string Rolename { get; set; }
        public int RoleId { get; set; }
        public string Username { get; set; }
        public int DepartmentId { get; set; }
        public int UserId { get; set; }
    }
}
