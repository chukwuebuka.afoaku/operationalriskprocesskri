﻿using OperationalRiskException.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Dtos
{
   public class AnswersDto
    {
       
        public int QuestionId { get; set; }
        public string QuestionTest { get; set; }
        public AnswerEnum? answerEnum { get; set; }
        public int AnswerValue { get; set; }
        public QuestionTypeEnum QuestionType { get; set; }
        public string AnswerTextA { get; set; }
        public string AnswerTextB { get; set; }
        public string AnswerTextC { get; set; }
        public string AnswerTextD { get; set; }
    }

    public class MainAnswerDto
    {
        public string FileDto { get; set; }
        public string CategoryName { get; set; }
       public List< AnswersDto> answers { get; set; }
        public int CategoryId { get; set; }
    }
}
