﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.AuthVM
{
    public class HRStaffObject
    {
        public int UserID { get; set; }
        public int RoleID { get; set; }
        public int status { get; set; }
        public string message { get; set; }
        public string id { get; set; }
        public string employeeNumber { get; set; }
        public string surname { get; set; }
        public string lastname { get; set; }
        public string Country2 { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string username { get; set; }
        public string Grade { get; set; }
        public string Role { get; set; }
        public string solID { get; set; }
        public string emailAddress { get; set; }
        public string directorateName { get; set; }
        public string divisionName { get; set; }
        public string sDivisionName { get; set; }
        public string departmentName { get; set; }
        public string departmentHeadName { get; set; }
        public string countryName { get; set; }
        public DateTime dateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Name { get; set; }
        public string MobileNumber { get; set; }
        public string StaffID { get; set; }
        public string StaffGrade { get; set; }
        public string AccountNumber { get; set; }
        public string Country { get; set; }
        public string CostCode { get; set; }
        public string ParentCode { get; set; }
        public string Department { get; set; }
        public string OfficeAddress { get; set; }
        public string BankName { get; set; }
        public string BankBranchID { get; set; }
        public string BankBranchName { get; set; }
        public string EmployeeID { get; set; }
        public string Sex { get; set; }
        public string CostCenter { get; set; }
        public string ParentCostCenter { get; set; }
        public string Designation { get; set; }
        public string SupervNo { get; set; }
        public string SupervName { get; set; }
        public string SupervGrade { get; set; }
        public string SupervDesignation { get; set; }
        public string SupervUser { get; set; }
        public string SecondSupervisorNo { get; set; }
        public string SecondSupervisorName { get; set; }
        public string SecondLGrade { get; set; }
        public string SecondLDesignation { get; set; }
        public string SecondSupervisorUser { get; set; }
        public string LeaveStatus { get; set; }
        public string ReliefName { get; set; }
        public string ReliefEmail { get; set; }
        public DateTime DateAdded { get; set; }
        public string RoleName { get; set; }
    }
}
