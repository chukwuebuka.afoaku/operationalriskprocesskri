﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Entities
{
   public class EscalationProgress :BaseEntity
    {
        public int Id { get; set; }
        public string SupervisorEmail { get; set; }
        public string SupervisorRole { get; set; }
        public int LevelOfEscalation { get; set; }
        public Escalation Escalation { get; set; }
        public int EscalationId { get; set; }
        public bool IsActive { get; set; }
    }
}
