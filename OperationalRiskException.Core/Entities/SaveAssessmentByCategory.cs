﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Entities
{
    public class SaveAssessmentByCategory:BaseEntity
    {
        public int Id { get; set; }
        public QuestionCategory Category { get; set; }
        public int CategoryId { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public int CategoryScore { get; set; }
        public int TotalQuestion{get; set;}
        public AssessmentSetup AssessmentSetup { get; set; }
        public int? AssessmentSetupId { get; set; }

    }
}
