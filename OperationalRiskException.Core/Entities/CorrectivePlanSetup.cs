﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Entities
{
  public class CorrectivePlanSetup : BaseEntity
    {
        public int Id { get; set; }
        public AssessmentSetup AssessmentSetup { get; set; }
        public int AssessmentSetupId { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
