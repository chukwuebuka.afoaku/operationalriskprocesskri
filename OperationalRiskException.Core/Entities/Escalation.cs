﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Entities
{
    public   class Escalation : BaseEntity
    {
       
        public int Id { get; set; }
        public int LevelOfEscalation { get; set; }
        public string ChampionEmail { get; set; }
        public bool HasEscalationStopped { get; set; }
        public User User{ get; set; }
        public int UserId { get; set; }
        public AssessmentScheduling AssessmentScheduler { get; set; }
        public int AssessmentSchedulerId { get; set; }

    }
}
