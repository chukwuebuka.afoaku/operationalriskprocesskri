﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web;
using Hangfire;
using Hangfire.Dashboard;
using Hangfire.SqlServer;
using Microsoft.Owin;
using OperationalRiskException.Services.Services;
using Owin;

[assembly: OwinStartup(typeof(OperationalRiskException.NotificationScheduler.Startup))]

namespace OperationalRiskException.NotificationScheduler
{
    public class Startup
    {
        private IEnumerable<IDisposable> GetHangfireServers()
        {
            string connstring = ConfigurationManager.ConnectionStrings["OperationalRiskContext"].ConnectionString;
            Hangfire.GlobalConfiguration.Configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()

                .UseSqlServerStorage(connstring, new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    UsePageLocksOnDequeue = true,
                    DisableGlobalLocks = true
                });

            yield return new BackgroundJobServer();
        }

        [Obsolete]
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            app.UseHangfireAspNet(GetHangfireServers);
            app
            .UseHangfireDashboard("/hangfire", new DashboardOptions
             {
                 Authorization = new[] { new MyAuthorizationFilter() }
             });
            // Let's also create a sample background job
            // BackgroundJob.Enqueue(() => Debug.WriteLine("Hello world from Hangfire!"));
            string tempPath=  HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["templatePath"] as string);
            var emailServices = new AssessmentNotificationService();
            RecurringJob.AddOrUpdate(() => emailServices.EmailSendingPeriodOfAssessment(tempPath), Cron.HourInterval(12));
        }
    }

    public class MyAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            // In case you need an OWIN context, use the next line, `OwinContext` class
            // is the part of the `Microsoft.Owin` package.
            var owinContext = new OwinContext(context.GetOwinEnvironment());

            // Allow all authenticated users to see the Dashboard (potentially dangerous).
            return true;
        }
    }
}
