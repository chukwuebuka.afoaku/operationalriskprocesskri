﻿using NLog;
using OperationalRiskException.AuditLogFilter;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OperationalRiskException.Controllers
{
    public class OperationalRiskController : Controller
    {
        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();
        // GET: OperationalRisk
        private IAssessmentPortalServices _assessmentPortalServices;
        private IDashboardService _dashboardService;

        public OperationalRiskController()
        {
            _assessmentPortalServices = new AssessmentPortalServices();
            _dashboardService = new DashboardService();
        }
        [LogActionFilter]
        public ActionResult Index()
        {
           var totalScore= _assessmentPortalServices.GetAllScores();
            return View(totalScore);
        }
        [LogActionFilter]
        public ActionResult UserScript(int userId, int SetupId)
        {
            var binder = new ScriptVM() {
                UserId=userId,
                SetupId=SetupId
            };

            return View(binder);
        }

        public ActionResult Dashboard()
        {
            var result = _dashboardService.DashBoard();
            return Json(new
            {
                data = result
            }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetUserRecords(int userId, int setupId)
        {
            var result = _assessmentPortalServices.GetAllSavedAssessments(userId,  setupId);
            return Json(new
            {
                data = result
            }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult UserCorrectivePlan(int userId, int SetupId)
        {

            var allRecords = _assessmentPortalServices.getAllCorrectivePlan(userId, SetupId);
            var binder = new ScriptVM()
            {
                UserId = userId,
                SetupId = SetupId,
                Plans=allRecords,
            };

            return View(binder);
        }
        public ActionResult GetUserCorrectiveRecords(int userId, int setupId)
        {
            var result = _assessmentPortalServices.GetAllSavedAssessments(userId, setupId);
            return Json(new
            {
                data = result
            }, JsonRequestBehavior.AllowGet);

        }


        public ActionResult UserGuide()
        {
            return View();
        }

    }
}