﻿using OperationalRiskException.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static OperationalRiskException.Core.ViewModels.ReportDisplays;

namespace OperationalRiskException.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        private IReportingService _report;
        public ReportController()
        {
            _report = new ReportingService();
        }
       
        public ActionResult Index()
        {
            var data = _report.GetApprovalWorkflowDetails();
            return View(data);
        }
        [HttpGet]
        public ActionResult ViewByDept(string dept)
        {
            ViewBag.Dept = dept;
            var data = _report.ViewResultsByDepartment(dept);
            return View(data);
        }


        [HttpPost]
        public ActionResult Index(ReportRequest _params)
        {
            var data = _report.GetApprovalWorkflowDetails();
            return View(data);
        }

    }
}