﻿using Dapper;
using FastMember;
using Newtonsoft.Json;
using NLog;
using OperationalRiskException.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static OperationalRiskException.Core.ViewModels.ReportDisplays;

namespace OperationalRiskException.Data.ReportRepo
{
    public interface IReportingRepo
    {
        ReportResponce GetApprovalWorkflowDetails();
        List<ReportModel> ViewResultsByDepartment(string dept);
    }

    public class ReportingRepo : IReportingRepo
    {
        readonly IDbConnection _sqldb;
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();
     
        public ReportingRepo()
        {
           
            _sqldb = ConnectionFactory.GetSqlConnection();

        }

     

        //[sp_ApprovalWorkFlowDetails]

        public  ReportResponce GetApprovalWorkflowDetails()

        {
            try
            {
                var sqlQuery = "[Sp_ScoreRecord]";
                var reports = new ReportDisplays();
                var labelHeaders = new List<header>();
                var body = new List<body>();

                var record = new FullReport();
                var usersResults = new List<ReportModel>();
                var headers = new List<HeaderView>();
                using (var multi =  _sqldb.QueryMultiple(sqlQuery, commandType: CommandType.StoredProcedure))
                {
                    headers = multi.Read<HeaderView>().ToList();
                    usersResults = multi.Read<ReportModel>().ToList();
                }
               

                DataTable table = new DataTable();
                string json = String.Empty;
                if (usersResults != null)
                {

                    //  var dict = new Dictionary<string, string>();
                    var dicList = new List<Dictionary<string, string>>();
                    var  diffDept=   usersResults.Select(x => x.DepartmentName).Distinct().ToList();
                    foreach (var dpObj in diffDept)
                    {
                        var dict = new Dictionary<string, string>();
                        dict.Add("BranchName", dpObj);
                        foreach(var list in headers)
                        {
                            var check = usersResults.Where(x => x.CategoryId == list.CategoryId && x.DepartmentName==dpObj).LastOrDefault();
                            if (check != null)
                            {
                                var PercentageScore = check.CategoryScore>0?((double)check.CategoryScore / (double)check.TotalQuestion * 100):0;
                                
                                dict.Add(list.CategoryName, PercentageScore.ToString() +"%");
                            }
                            else
                            {
                                dict.Add(list.CategoryName, "-");
                            }

                        }
                     
                        dicList.Add(dict);
                      

                    }
                    json = JsonConvert.SerializeObject(dicList);
                    table=(DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));

                }
                return ConvertDataTableToView(table);
            }
            catch (Exception exx)
            {
                logger.Error("IssuewithResponse:::" + exx.ToString());
                throw new Exception(exx.ToString());
            }
        }
        private ReportResponce ConvertDataTableToView(DataTable dt)
        {
            int i = 0;
            var headers = new List<ReportDisplays.header>();
            foreach (DataColumn column in dt.Columns)
            {
                headers.Add(new ReportDisplays.header
                {
                    value = column.ColumnName,
                    key = column.ColumnName,
                });

                i++;
            }


            var bodies = new List<List<ReportDisplays.body>>();
            foreach (DataRow row in dt.Rows)
            {
                var bodie = new List<ReportDisplays.body>();
                foreach (DataColumn column in dt.Columns)
                {
                    bodie.Add(new ReportDisplays.body
                    {
                        value = row[column].ToString(),
                        key = column.ColumnName
                    });
                }
                bodies.Add(bodie);
            }

            return new ReportDisplays.ReportResponce
            {
                tableHeader = headers,
                tableValue = bodies
            };
        }

        public List<ReportModel> ViewResultsByDepartment(string dept)
        {
            var sqlQuery = "[Sp_SingleReportRecord]";

            var result = _sqldb.Query<ReportModel>(sqlQuery,new { DepartmentName=dept }, commandType: CommandType.StoredProcedure);
            return result.ToList();
        }
    }
}
