﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Data
{
    public static class ConnectionFactory
    {

        public static SqlConnection GetSqlConnection()
        {
            return new SqlConnection(ConfigurationManager.ConnectionStrings["OperationalRiskKRIContext"].ConnectionString);
        }

        public static SqlConnection GetSqlConnectionApproval()
        {
            return new SqlConnection(ConfigurationManager.ConnectionStrings["HRDBContext"].ConnectionString);
        }
    }
}
