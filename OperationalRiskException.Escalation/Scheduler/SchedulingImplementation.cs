﻿using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Services;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OperationalRiskException.Escalation.Scheduler
{
    public class SchedulingImplementation : IJob
    {
        private IEscalationService _escalationService;
        public SchedulingImplementation()
        {
            _escalationService = new EscalationService(); ;

        }

        public void Execute(IJobExecutionContext context)
        {
            _escalationService.FirstEscalationImplementation();

        }
    }

    public class OtherSchedulingImplementation : IJob
    {
        private IEscalationService _escalationService;
        public OtherSchedulingImplementation()
        {
            _escalationService = new EscalationService(); ;

        }

        public void Execute(IJobExecutionContext context)
        {
            _escalationService.ContinuousEscalationImplementation();

        }
    }

    public class SoftWarningSchedulingImplementation : IJob
    {
        private IEscalationService _escalationService;
        public SoftWarningSchedulingImplementation()
        {
            _escalationService = new EscalationService(); ;

        }

        public void Execute(IJobExecutionContext context)
        {
            _escalationService.SoftWarningEscalation();

        }
    }
}